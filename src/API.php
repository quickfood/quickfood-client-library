<?php

namespace Quickfood\ClientLibrary;

use \Httpful\Request;

/**
 * API client.
 */
class API
{
    /**
     * Hashing algorithm used for HMAC
     * 
     * @var string
     */
    const HMAC_HASH_ALGO = "sha256";
    
    /**
     * Api url to issue the requests
     * 
     * @var string
     */
    protected $url = "https://api-sweden.stable.qfdev.net/v2";
    
    /**
     * Timeout of the request in seconds
     * 
     * @var integer
     */
    protected $requestTimeout = 60;
    
    /**
     * The number of times to retry in case of response code
     * being 500 or higher
     *
     * @var integer
     */
    protected $maxRetries = 1;
    
    /**
     * The number of seconds to sleep between retries
     *
     * @var integer
     */
    protected $sleepBetweenRetries = 1;
    
    /**
     * Public API key
     * 
     * @var string
     */
    protected $publicKey;

    /**
     * Private API key
     * 
     * @var string
     */
    protected $privateKey;
    
    /**
     * Extra headers
     * 
     * @var array
     */    
    protected $extraHeaders = [];

    /**
     * Construct.
     *
     * @param string $publicKey  The public key for the authorization.
     * @param string $privateKey The private key for the authorization.
     */
    public function __construct($publicKey, $privateKey)
    {
        $this->publicKey = $publicKey;
        $this->privateKey = $privateKey;
    }
    
    /**
     * Set the base url for the api
     *
     * @param string $url The url
     */
    public function setUrl($url)
    {   
        $this->url = $url;
    }
    
    /**
     * Set the request timeout
     *
     * @param integer $timeout The request timeout (in seconds)
     */
    public function setRequestTimeout($timeout)
    {   
        $this->requestTimeout = $timeout;
    }
    
    /**
     * Set the extra headers
     *
     * @param array $headers The extra headers
     */
    public function setExtraHeaders($headers)
    {   
        $this->extraHeaders = $headers;
    }    
    
    /**
     * Set max tries for the api
     *
     * @param integer The number of times to retry in case of response code being 500 or higher
     */
    public function setMaxRetries($maxRetries)
    {   
        $this->maxRetries = $maxRetries;
    }

    /**
     * Request a resource from the API using the GET method.
     * 
     * @param string $resourcePath The path, e.g. /cities
     * @param array  $params       The query parameters (associative array)
     *
     * @return \Httpful\Response The response from the API
     */
    public function get($resourcePath, $params = [])
    {
        return $this->call("GET", $resourcePath, $params);
    }
    
    /**
     * Request a resource from the API using the POST method.
     * 
     * @param string $resourcePath The path
     * @param array  $payload      The payload (associative array)
     *
     * @return \Httpful\Response The response from the API
     */    
    public function post($resourcePath, $payload = [])
    {
        return $this->call("POST", $resourcePath, $payload);
    }
  
    /**
     * Request a resource from the API using the PUT method.
     * 
     * @param string $resourcePath The path
     * @param array  $payload      The payload (associative array)
     *
     * @return \Httpful\Response The response from the API
     */ 
    public function put($resourcePath, $payload = [])
    {
        return $this->call("PUT", $resourcePath, $payload);
    }

    /**
     * Request a resource from the API using the PATCH method.
     * 
     * @param string $resourcePath The path
     * @param array  $payload      The payload (associative array)
     *
     * @return \Httpful\Response The response from the API
     */ 
    public function patch($resourcePath, $payload = [])
    {
        return $this->call("PATCH", $resourcePath, $payload);
    }
    
    /**
     * Request a resource from the API using the DELETE method.
     * 
     * @param string $resourcePath The path
     * @param array  $params       The query parameters (associative array)
     *
     * @return \Httpful\Response The response from the API
     */ 
    public function delete($resourcePath, $params = [])
    {
        return $this->call("DELETE", $resourcePath, $params);
    }
    
    /**
     * Generate headers to use for request to API
     * 
     * @return array The headers
     */
    protected function generateHeaders()
    {
        $request = \Symfony\Component\HttpFoundation\Request::createFromGlobals();

        $nonce = $this->getNonce();

        $headers = [];
        $headers["X-API-KEY"] = $this->publicKey;
        $headers["X-API-NONCE"] = $nonce;
        $headers["X-API-HASH"] = $this->generateHMACHash($nonce);
        $headers["X-REAL-IP"] = !empty($request->headers->get('x-real-ip'))
                                ? $request->headers->get('x-real-ip')
                                : $request->server->get('REMOTE_ADDR');

        return $headers + $this->extraHeaders;
    }

    /**
     * Generates unique nonce.
     * 
     * @return string The nonce
     */
    protected function getNonce()
    {
        return hash("sha1", openssl_random_pseudo_bytes(32));
    }

    /**
     * Generates HMAC hash which will allow client contact the API.
     *
     * @param string $nonce The nonce.
     * 
     * @return string Hash calculated used HMAC algorithm as API.
     */
    protected function generateHMACHash($nonce)
    {
        return hash_hmac(self::HMAC_HASH_ALGO, ($this->publicKey . $nonce), $this->privateKey);
    }
    
    /**
     * Call
     * 
     * @param array  $headers       Additional headers to add to the request.
     * @param string $method        The request method (GET, POST, PUT, DELETE).
     * @param string $resourcePath  The resource to use for issuing the request.
     * @param array  $data          Additional data to add to the request.
     * 
     * @return mixed The response
     * 
     * @throws \Exception
     */
    protected function call($method, $resourcePath, $data)
    {
        // issue the request.
        // if we have an internal server error, try to issue the request again,
        // up to maximum tries
        $tries = 0;
        do {
            // keep creations of request object inside the loop
            // otherwise if initial response is 500 then for a reason
            // we are not calling api again. Panos
            $headers = $this->generateHeaders();

            // assign url to variable
            $url = $this->url;

            // append resource
            $url .= $resourcePath;

            // create a request object
            switch ($method) {
                case "GET":
                    // append data as query parameters
                    if (!empty($data)) {
                        $extra = [];

                        foreach ($data as $pKey => $pValue) {
                            $extra[] = $pKey . "=" . $pValue;
                        }

                        $url .= "?" . implode("&", $extra);
                    }

                    $request = Request::get($url);
                    break;
                case "POST":
                    $request = Request::post($url);
                    break;
                case "PUT":
                    $request = Request::put($url);
                    break;
                case "PATCH":
                    $request = Request::patch($url);
                    break;
                case "DELETE":
                    $request = Request::delete($url);
                    break;
                default:
                    throw new \Exception("Abstract client does not support method '{$method}'.");
            }

            // add $data to body in case of POST/PUT
            if (!empty($data) && in_array($method, ["POST", "PUT", "PATCH"])) {
                $request->body($data)->sendsJson();
            }

            // add the headers (optional)
            if (!empty($headers)) {
                $request->addHeaders($headers);
            }

            // set timeout (optional)
            if (isset($this->requestTimeout)) {
                $request->timeout($this->requestTimeout);
            }

            if ($tries !== 0) {
                sleep($this->sleepBetweenRetries);
            }
            try {
                $response = $request->send();
            } catch (\Exception $e) {
                // seems that Httpful throws an exception if http code is >= 500
                // catch it and do nothing
            }

            $tries++;
        } while ($tries <= $this->maxRetries 
                && $response->code >= Constants::HTTP_CODE_INTERNAL_SERVER_ERROR);

        return $response;
    }
}
