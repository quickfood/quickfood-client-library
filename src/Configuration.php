<?php

namespace Quickfood\ClientLibrary;

/**
 * Configuration class file.
 * Parses yaml file and returns it's data.
 * If you want to add other configuration parameters, you can add them here.
 */
class Configuration
{
    /**
     * Load and parse configuration file.
     *
     * @param string $yamlFile The configuration file to load.
     *
     * @return array The app configuration.
     */
    public static function load($yamlFile)
    {
        $configuration = array();

        // parse yaml file
        $yamlContents = yaml_parse_file($yamlFile);

        if (!empty($yamlContents)) {
            $configuration = $yamlContents;
        }

        return $configuration;
    }
}
