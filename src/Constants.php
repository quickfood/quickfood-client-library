<?php

namespace Quickfood\ClientLibrary;

/**
 * Constants class file.
 *
 * Contains constant variables for the library
 */
class Constants
{
    const HTTP_CODE_OK = 200;
    const HTTP_CODE_CREATED = 201;
    const HTTP_CODE_NO_CONTENT = 204;
    const HTTP_CODE_MOVED_PERMANENTLY = 301;
    const HTTP_CODE_FOUND = 302;
    const HTTP_CODE_BAD_REQUEST = 400;
    const HTTP_CODE_UNAUTHORIZED = 401;
    const HTTP_CODE_FORBIDDEN = 403;
    const HTTP_CODE_NOT_FOUND = 404;
    const HTTP_CODE_METHOD_NOT_ALLOWED = 405;
    const HTTP_CODE_CONFLICT = 409;
    const HTTP_CODE_PRECONDITION_FAILED = 412;
    const HTTP_CODE_UNSUPPORTED_MEDIA_TYPE = 415;
    const HTTP_CODE_UNPROCESSABLE_ENTITY = 422;
    const HTTP_CODE_INTERNAL_SERVER_ERROR = 500;
    const HTTP_CODE_NOT_IMPLEMENTED = 501;
    const HTTP_CODE_BAD_GATEWAY = 502;
    const HTTP_CODE_TEMPORARILY_UNAVAILABLE = 503;
}
