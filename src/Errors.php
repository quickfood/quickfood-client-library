<?php

namespace Quickfood\ClientLibrary;

use Quickfood\ClientLibrary\Constants;

/**
 * Errors class. Used to handle API generated errors
 * based on internal error response structure and
 * translate them if possible to current language.
 */
class Errors
{
    /**
     * Internal name for fatal types of errors.
     *
     * @var string
     */
    const ERROR_TYPE_FATAL = "_error_type_fatal";

    /**
     * Internal name for normal types of errors.
     *
     * @var string
     */
    const ERROR_TYPE_NORMAL = "_error_type_normal";

    /**
     * Internal name for validation types of errors.
     *
     * @var string
     */
    const ERROR_TYPE_VALIDATION = "_error_type_validation";

    /**
     * Will be used to translate error messages.
     *
     * @var \Symfony\Component\Translation\TranslatorInterface
     */
    private $translator;

    /**
     * Construct.
     *
     * @param \Symfony\Component\Translation\TranslatorInterface $translator An instance of TranslatorInterface
     */
    public function __construct(\Symfony\Component\Translation\TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * Get error message from the response.
     * Tries to translate it if possible.
     *
     * @param \Httpful\Response $response  Response from the API
     * @param string            $category  Optional name for categorizing the request
     *
     * @return string
     */
    public function getErrorMessage($response, $category = null)
    {
        $errorCode = $response->body->error_code;
        $errorMsg = $response->body->msg;

        $transKey = [];

        $transKey[] = $errorCode;
        
        if (!empty($category)) {
            $transKey[] = $category;
        }

        return $this->findMatchingTranslation($transKey, $errorMsg);
    }

    /**
     * Get error type from the response http code.
     * 
     * @param \Httpful\Response $response   Response from the API
     *
     * @return string
     */
    public function getErrorType($response)
    {
        switch ($response->code) {
            case Constants::HTTP_CODE_BAD_REQUEST:
            case Constants::HTTP_CODE_METHOD_NOT_ALLOWED:
            case Constants::HTTP_CODE_UNSUPPORTED_MEDIA_TYPE:
            case Constants::HTTP_CODE_INTERNAL_SERVER_ERROR:
            case Constants::HTTP_CODE_NOT_IMPLEMENTED:
            case Constants::HTTP_CODE_BAD_GATEWAY:
            case Constants::HTTP_CODE_TEMPORARILY_UNAVAILABLE:
                $errorType = self::ERROR_TYPE_FATAL;
                break;
            case Constants::HTTP_CODE_UNPROCESSABLE_ENTITY:
                $errorType = self::ERROR_TYPE_VALIDATION;
                break;
            default:
                $errorType = self::ERROR_TYPE_NORMAL;
                break;
        }
        
        return $errorType;
    }

    /**
     * Processes validation errors which are following the structure:
     *
     * [field] => Array
     *     (
     *         [0] => Array
     *             (
     *                 [message] => This value is not a valid email.
     *                 [value] => t.fotisactonbit.gr
     *             )
     *     )
     *
     * Tries to translate message of each field it if possible.
     *
     * @param \Httpful\Response $response  Response from the API
     * @param string            $category  Optional name for categorizing the request
     *
     * @return string
     */
    public function processValidationErrors($response, $category = null)
    {
        $errorCode = isset($response->body->error_code) ? $response->body->error_code : Constants::HTTP_CODE_INTERNAL_SERVER_ERROR;
        $generalError = isset($response->body->msg) ? $response->body->msg : null;
        $errorDevMsg = isset($response->body->dev_msg) ? json_decode($response->body->dev_msg, true) : null;

        // if it's not an array at least return the string.
        if (!is_array($errorDevMsg)) {
            $transKey = [];
            
            $transKey[] = $errorCode;

            if (!empty($category)) {
                $transKey[] = $category;
            }

            return [
                "errorCode" => $errorCode,
                "generalError" => $generalError,
                "validationErrors" => $this->findMatchingTranslation($transKey, $errorDevMsg)
            ];
        }

        $validationErrors = [];
        foreach ($errorDevMsg as $field => $error) {
            $validationErrors[$field] = $error;

            foreach ($error as $key => $item) {
                if (is_array($item)) {
                    $item = $this->findErrorBodyRecursive($item);
                    $key = 0;
                }
                
                if (!empty($item["message"])) {
                    $errorMsg = $item["message"];

                    $transKey = [];

                    $transKey[] = $errorCode;
                    
                    if (!empty($category)) {
                        $transKey[] = $category;
                    }
                    
                    $transKey[] = $errorMsg;

                    $item["translation"] = $this->findMatchingTranslation($transKey, $errorMsg);
                }

                $validationErrors[$field][$key] = $item;
            }
        }

        return [
            "errorCode" => $errorCode,
            "generalError" => $generalError,
            "validationErrors" => $validationErrors
        ];
    }

    /**
     * Build different combinations of translation keys in order
     * to find best matching translation.
     * If not possible, fallbacks to $errorMsg which supposed to be in english.
     *
     * @param array  $transKey   Pre-built array of translation keys
     * @param string $errorMsg   Internal error message from API.
     *
     * @return string
     */
    protected function findMatchingTranslation($transKey, $errorMsg)
    {
        // Build the key which will be used in finding the translation.
        $transKeyStr = implode('.', $transKey);
        $transMessage = $this->translator->trans($transKeyStr);
        
        if ($transMessage == $transKeyStr) {
            // Haven't found translation based on full transKey.
            // Will try to fallback to errorCode's default translation.
            $transKey[1] = "default";
            $transKeyStr = implode(".", $transKey);
            $transMessage = $this->translator->trans($transKeyStr);

            if ($transMessage == $transKeyStr) {
                // If not found even by only errorCode falling back
                // to only translated message and if that is not
                // translated generically it will be displayed in english.
                $transMessage = $this->translator->trans($errorMsg);
            }
        }

        return $transMessage;
    }

    /**
     * In case of nested properties (e.g. discounts.student.card_number)
     * we have to find the actual error contents recursively.
     *
     * @param array $errorItem The error item
     *
     * @return array
     */
    protected function findErrorBodyRecursive($errorItem)
    {
        if (isset($errorItem['message'])) {
            return $errorItem;
        }

        foreach ($errorItem as $subItem) {
            return $this->findErrorBodyRecursive($subItem);
        }
    }
}
